package InterfaceServices;

import ServiceClasses.Employee;

public interface EmployeeService {
    void register(Employee[] employees);
    void show();
    void update();
    void delete();
    Employee findByName(String name);
    int getTotalEmployee();
}

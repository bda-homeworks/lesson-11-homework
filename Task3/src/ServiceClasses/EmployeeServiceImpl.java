package ServiceClasses;

import InterfaceServices.EmployeeService;

import java.util.Scanner;

public class EmployeeServiceImpl implements EmployeeService {
    private Employee[] globalData;
    private int employeeCount;
    private static final int MAX_SIZE= 100;

    @Override
    public void register(Employee[] globalData) {
        for (Employee employee : globalData) {
            if (employeeCount < MAX_SIZE) {
                this.globalData[employeeCount++] = employee;
                System.out.println("Employee registered successfully");
            } else {
                System.out.println("Cannot register more employees.");
                break;
            }
        }
    }

    @Override
    public void show() {
        if (employeeCount == 0) {
            System.out.println("No employee found");
        }

        for (int i = 0; i < employeeCount; i++) {
            Employee employee = globalData[i];
            System.out.println("Name: " + employee.getName());
            System.out.println("Surname: " + employee.getSurname());
            System.out.println("Age: " + employee.getAge());
            System.out.println("Position: " + employee.getPosition());
            System.out.println("Salary: " + employee.getSalary());
            System.out.println("-----------------------");
        }
    }

    @Override
    public void update() {

    }

    @Override
    public void delete() {

    }

    @Override
    public Employee findByName(String name) {
        return null;
    }

    @Override
    public int getTotalEmployee() {
        return 0;
    }

    public static void main(String[] args) {
        EmployeeServiceImpl employeeService = new EmployeeServiceImpl();

        Employee[] globalData = new Employee[2];

        Scanner input = new Scanner(System.in);

        for (int i = 0; i < globalData.length; i++) {
            System.out.println("Enter the details for Employee " + (i + 1));
            System.out.print("Name: ");
            String name = input.next();
            System.out.print("Surname: ");
            String surname = input.next();
            System.out.print("Age: ");
            int age = input.nextInt();
            System.out.print("Position: ");
            String position = input.next();
            System.out.print("Salary: ");
            int salary = input.nextInt();

            Employee employee = new Employee(name, surname, age, position, salary);
            globalData[i] = employee;
        }

        employeeService.register(globalData);

        employeeService.show();

        input.close();
    }
}

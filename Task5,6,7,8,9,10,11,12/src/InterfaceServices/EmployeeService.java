package InterfaceServices;

import ClassServices.Employee;

public interface EmployeeService {
    void register(Employee[] employees);
    void show();
    void update(String name, Employee updatedEmployee);
    void delete(int id);
    Employee findByName(String name);
    int getTotalEmployee();
}

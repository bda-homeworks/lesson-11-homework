package ClassServices;

import InterfaceServices.EmployeeService;

import java.util.Scanner;

public class EmployeeServiceImpl implements EmployeeService {
    private static final int MAX_SIZE = 100;
    private Employee[] employees;
    private int employeeCount;

    public EmployeeServiceImpl() {
        this.employees = new Employee[MAX_SIZE];
        this.employeeCount = 0;
    }

    @Override
    public void register(Employee[] employees) {
            for (Employee employee : employees) {
                if (employeeCount < MAX_SIZE) {
                    this.employees[employeeCount++] = employee;
                    System.out.println("Employee registered succesfully");
                } else {
                    System.out.println("Cannot register more employees. Limit is approached.");
                    break;
                }
            }
    }

    @Override
    public void show() {
        if (employeeCount == 0) {
            System.out.println("No employees found.");
        } else {
            for (int i = 0; i < employeeCount; i++) {
                Employee employee = employees[i];
                System.out.println("ID: " + employee.getId());
                System.out.println("Name: " + employee.getName());
                System.out.println("Surname: " + employee.getSurname());
                System.out.println("Age: " + employee.getAge());
                System.out.println("Position: " + employee.getPosition());
                System.out.println("Salary: " + employee.getSalary());
                System.out.println("-----------------------");
            }
        }
    }

    @Override
    public void update(String name, Employee updatedEmployee) {
        boolean found = false;
        for (int i = 0; i < employeeCount; i++) {
            if (employees[i].getName().equalsIgnoreCase(name)) {
                employees[i] = updatedEmployee;
                System.out.println("Employee with name: " + name + " updated succesfully.");
                found = true;
                break;
            }
        }
        if (!found) {
            System.out.println("Employee with the name: " + name + " not found");
        }
    }

    @Override
    public void delete(int id) {
        int deleteIndex = -1;
        for (int i = 0; i < employeeCount; i++) {
            if (employees[i].getId() == id) {
                deleteIndex = i;
                break;
            }
        }

        if (deleteIndex != -1) {
            for (int i = deleteIndex; i < employeeCount - 1; i++) {
                employees[i] = employees[i + 1];
            }
            employeeCount--;
            System.out.println("Employee with ID " + id + " deleted successfully");
        } else {
            System.out.println("Employee with ID " + id + " not found.");
        }
    }

    @Override
    public Employee findByName(String name) {
        for (int i = 0; i < employeeCount; i++) {
            if (employees[i].getName().equalsIgnoreCase(name)) {
                return employees[i];
            }
        }
        return null;
    }

    @Override
    public int getTotalEmployee() {
        return employeeCount;
    }


    public static void main(String[] args) {
        EmployeeServiceImpl employeeService = new EmployeeServiceImpl();
        Scanner input = new Scanner(System.in);
        boolean continueLoop = true;


        while (continueLoop) {
            System.out.println("1 - Register");
            System.out.println("2 - Show Employees");
            System.out.println("3 - Update Employee");
            System.out.println("4 - Delete Employee");
            System.out.println("5 - Find Employee by Name");
            System.out.println("6 - Get Total Employees");
            System.out.println("7 - Exit");
            System.out.print("Enter your choice: ");
            int choice = input.nextInt();

            switch (choice) {
                case 1:
                    int numEmployees = 1;

                    Employee[] employees = new Employee[numEmployees];
                    for (int i = 0; i < numEmployees; i++) {
                        System.out.println("Enter details for Employee:");
                        System.out.print("Name: ");
                        String name = input.next();
                        System.out.print("Surname: ");
                        String surname = input.next();
                        System.out.print("Age: ");
                        int age = input.nextInt();
                        System.out.print("Position: ");
                        String position = input.next();
                        System.out.print("Salary: ");
                        int salary = input.nextInt();
                        input.nextLine();

                        Employee employee = new Employee(name, surname, age, position, salary);
                        employees[i] = employee;
                    }

                    employeeService.register(employees);
                    break;
                case 2:
                    employeeService.show();
                    break;
                case 3:
                    System.out.println("Enter the name of the employee to update");
                    String updateName = input.next();

                    Employee existingEmployee = employeeService.findByName(updateName);
                    if (existingEmployee != null) {
                        System.out.println("Enter new details for the employee:");
                        System.out.print("Name: ");
                        String newName = input.next();
                        System.out.print("Surname: ");
                        String newSurname = input.next();
                        System.out.print("Age: ");
                        int newAge = input.nextInt();
                        input.nextLine(); // Consume the newline character
                        System.out.print("Position: ");
                        String newPosition = input.next();
                        System.out.print("Salary: ");
                        int newSalary = input.nextInt();
                        input.nextLine(); // Consume the newline character

                        Employee updatedEmployee = new Employee(newName, newSurname, newAge, newPosition, newSalary);
                        employeeService.update(updateName, updatedEmployee);
                    } else {
                        System.out.println("Employee with name '" + updateName + "' not found.");
                    }
                    break;
                case 4:
                    System.out.println("----------------------------------");
                    System.out.print("Enter the ID of the employee to delete: ");
                    int deleteId = input.nextInt();
                    System.out.println("----------------------------------");

                    employeeService.delete(deleteId);
                    break;
                case 5:
                    System.out.println("----------------------------------");
                    System.out.print("Enter the name of the employee to find: ");
                    String findName = input.next();

                    Employee foundEmployee = employeeService.findByName(findName);
                    if (foundEmployee != null) {
                        System.out.println("Employee found:");
                        System.out.println("ID: " + foundEmployee.getId());
                        System.out.println("Name: " + foundEmployee.getName());
                        System.out.println("Surname: " + foundEmployee.getSurname());
                        System.out.println("Age: " + foundEmployee.getAge());
                        System.out.println("Position: " + foundEmployee.getPosition());
                        System.out.println("Salary: " + foundEmployee.getSalary());
                    } else {
                        System.out.println("----------------------------------");
                        System.out.println("Employee with name '" + findName + "' not found.");
                        System.out.println("----------------------------------");
                    }
                    break;
                case 6:
                    int totalEmployees = employeeService.getTotalEmployee();
                    System.out.println("----------------------------------");
                    System.out.println("Total Employees: " + totalEmployees);
                    System.out.println("----------------------------------");
                    break;
                case 7:
                    continueLoop = false;
                    break;
                default:
                    System.out.println("----------------------------------");
                    System.out.println("Invalid choice. Please try again!");
                    System.out.println("----------------------------------");
                    break;
            }
        }
    }
}

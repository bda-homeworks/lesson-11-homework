package ClassServices;

import InterfaceServices.EmployeeService;

import java.util.Scanner;

public class EmployeeServiceImpl implements EmployeeService {
    private static final int MAX_SIZE = 100;
    private Employee[] employees;
    private int employeeCount;

    public EmployeeServiceImpl() {
        this.employees = new Employee[MAX_SIZE];
        this.employeeCount = 0;
    }

    @Override
    public void register(Employee[] employees) {
            for (Employee employee : employees) {
                if (employeeCount < MAX_SIZE) {
                    this.employees[employeeCount++] = employee;
                    System.out.println("Employee registered succesfully");
                } else {
                    System.out.println("Cannot register more employees. Limit is approached.");
                    break;
                }
            }
    }

    @Override
    public void show() {
        if (employeeCount == 0) {
            System.out.println("No employees found.");
        } else {
            for (int i = 0; i < employeeCount; i++) {
                Employee employee = employees[i];
                System.out.println("ID: " + employee.getId());
                System.out.println("Name: " + employee.getName());
                System.out.println("Surname: " + employee.getSurname());
                System.out.println("Age: " + employee.getAge());
                System.out.println("Position: " + employee.getPosition());
                System.out.println("Salary: " + employee.getSalary());
                System.out.println("-----------------------");
            }
        }
    }











    public static void main(String[] args) {
        EmployeeServiceImpl employeeService = new EmployeeServiceImpl();
        Scanner input = new Scanner(System.in);
        boolean continueLoop = true;


        while (continueLoop) {
            System.out.println("1 - Register");
            System.out.println("1 - Show Employees");
            System.out.println("3 - Exit");
            System.out.print("Enter your choice: ");
            int choice = input.nextInt();

            switch (choice) {
                case 1:
                    int numEmployees = 1;

                    Employee[] employees = new Employee[numEmployees];
                    for (int i = 0; i < numEmployees; i++) {
                        System.out.println("Enter details for Employee:");
                        System.out.print("Name: ");
                        String name = input.next();
                        System.out.print("Surname: ");
                        String surname = input.next();
                        System.out.print("Age: ");
                        int age = input.nextInt();
                        System.out.print("Position: ");
                        String position = input.next();
                        System.out.print("Salary: ");
                        int salary = input.nextInt();
                        input.nextLine();

                        Employee employee = new Employee(name, surname, age, position, salary);
                        employees[i] = employee;
                    }

                    employeeService.register(employees);
                    break;
                case 2:
                    employeeService.show();
                    break;
                case 3:
                    continueLoop = false;
                    break;
                default:
                    System.out.println("Invalid choice. Please try again!");
                    break;
            }
        }
    }
}

package InterfaceServices;

import ClassServices.Employee;

public interface EmployeeService {
    void register(Employee[] employees);
    void show();
}

package ServiceClasses;

import InterfaceServices.EmployeeService;

public class EmployeeServiceImpl implements EmployeeService {
    @Override
    public void register() {
        System.out.println("Being overriden");
    }

    @Override
    public void show() {
        System.out.println("Being overriden");

    }

    @Override
    public void update() {
        System.out.println("Being overriden");

    }

    @Override
    public void delete() {
        System.out.println("Being overriden");

    }

    @Override
    public void findByName() {
        System.out.println("Being overriden");

    }

    @Override
    public void getTotalEmployee() {
        System.out.println("Being overriden");

    }
}

package InterfaceServices;

public interface EmployeeService {
    void register();
    void show();
    void update();
    void delete();
    void findByName();
    void getTotalEmployee();
}

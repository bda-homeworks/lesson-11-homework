package service;

import phone.Phone;
import phone.dto.PhoneDTO;

import java.util.ArrayList;
import java.util.List;

public class PhoneService {
    private List<Phone> phones;

    public PhoneService() {
        this.phones = new ArrayList<>();
    }

    public void create(PhoneDTO phoneDTO) {
        Phone phone = new Phone(phoneDTO.getId(), phoneDTO.getBrand(), phoneDTO.getModel());
        phones.add(phone);
    }

    public List<Phone> showAll() {
        return phones;
    }

    public List<Phone> showAll(boolean withId) {
        List<Phone> result = new ArrayList<>();
        if (withId) {
            for (Phone phone : phones) {
                if (phone.getId() != null) {
                    result.add(phone);
                }
            }
        } else {
            result.addAll(phones);
        }
        return result;
    }

    public void delete(String id) {
        Phone phoneToRemove = null;
        for (Phone phone : phones) {
            if (phone.getId().equals(id)) {
                phoneToRemove = phone;
                break;
            }
        }
        if (phoneToRemove != null) {
            phones.remove(phoneToRemove);
        }
    }
}
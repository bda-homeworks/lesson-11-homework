import phone.Phone;
import phone.dto.PhoneDTO;
import service.PhoneService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static PhoneService phoneService = new PhoneService();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int choice;

        do {
            System.out.println("1. Create a new phone");
            System.out.println("2. Show all phones");
            System.out.println("3. Show all phones with ID");
            System.out.println("4. Delete a phone");
            System.out.println("0. Exit");
            System.out.print("Enter your choice: ");
            choice = scanner.nextInt();
            scanner.nextLine(); // Consume the newline character

            switch (choice) {
                case 1:
                    createPhone(scanner);
                    break;
                case 2:
                    showAllPhones();
                    break;
                case 3:
                    showAllPhones(true);
                    break;
                case 4:
                    deletePhone(scanner);
                    break;
                case 0:
                    System.out.println("Exiting...");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
                    break;
            }

            System.out.println();
        } while (choice != 0);
    }

    private static void createPhone(Scanner scanner) {
        System.out.print("Enter the phone ID: ");
        String id = scanner.nextLine();
        System.out.print("Enter the brand: ");
        String brand = scanner.nextLine();
        System.out.print("Enter the model: ");
        String model = scanner.nextLine();

        PhoneDTO phoneDTO = new PhoneDTO(id, brand, model);
        phoneService.create(phoneDTO);
        System.out.println("Phone created successfully.");
    }

    private static void showAllPhones() {
        List<Phone> phones = phoneService.showAll();
        if (phones.isEmpty()) {
            System.out.println("No phones found.");
        } else {
            for (Phone phone : phones) {
                System.out.println("ID: " + phone.getId() + ", Brand: " + phone.getBrand() + ", Model: " + phone.getModel());
            }
        }
    }

    private static void showAllPhones(boolean withId) {
        List<Phone> phones = phoneService.showAll(withId);
        if (phones.isEmpty()) {
            System.out.println("No phones found.");
        } else {
            for (Phone phone : phones) {
                System.out.println("ID: " + phone.getId() + ", Brand: " + phone.getBrand() + ", Model: " + phone.getModel());
            }
        }
    }

    private static void deletePhone(Scanner scanner) {
        System.out.print("Enter the phone ID to delete: ");
        String id = scanner.nextLine();

        phoneService.delete(id);
        System.out.println("Phone deleted successfully.");
    }
}

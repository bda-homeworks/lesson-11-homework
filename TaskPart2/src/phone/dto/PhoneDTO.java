package phone.dto;

public class PhoneDTO {
    private String id;
    private String brand;
    private String model;

    public PhoneDTO(String id, String brand, String model) {
        this.id = id;
        this.brand = brand;
        this.model = model;
    }

    public String getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }
}
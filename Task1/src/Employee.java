import java.util.Scanner;

public class Employee extends Personal{
    public Employee(String name, String surname, int age) {
        super(name, surname, age);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter the name: ");
        String nameInput = input.next();
        System.out.print("Enter the surname: ");
        String surnameInput = input.next();
        System.out.print("Enter the name: ");
        int ageInput = input.nextInt();

        Employee isci = new Employee(nameInput, surnameInput, ageInput);

        System.out.println(isci.getName() + " " + isci.getSurname() + " " + isci.getAge());
    }
}
